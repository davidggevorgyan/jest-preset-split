const fs = require( 'fs' );

module.exports = async () => {
	if ( process.env.SPLITKEY ) {
		fs.unlinkSync( './splitio.json' );
	}
};
