const { SplitFactory } = require( '@splitsoftware/splitio' );
const fs = require( 'fs' );


// eslint-disable-next-line consistent-return
module.exports = async () => new Promise( ( resolve, reject ) => {
	if ( !process.env.SPLITKEY ) {
		return resolve( 'No API Key found' );
	}
	let splitNames;
	let splitNamesAndValues;
	const factory = SplitFactory( {
		core: {
			authorizationKey: process.env.SPLITKEY,
			key: 'key',
		},
		startup: {
			readyTimeout: 1.5,
		},
	} );

	const manager = factory.manager();
	manager.once( manager.Event.SDK_READY, () => {
		splitNames = manager.names();
	} );
	manager.once( manager.Event.SDK_READY_TIMED_OUT, ( error ) => reject( error ) );

	const client = factory.client();
	client.on( client.Event.SDK_READY, () => {
		splitNamesAndValues = client.getTreatments( 'key', splitNames );
		fs.writeFileSync( './splitio.json', JSON.stringify( splitNamesAndValues ) );
		client.destroy();
		return resolve( splitNamesAndValues );
	} );
	client.on( client.Event.SDK_READY_TIMED_OUT, ( error ) => {
		client.destroy();
		return reject( error );
	} );
} );
