const NodeEnvironment = require( 'jest-environment-node' );
const fs = require( 'fs' );

class ReadSplits extends NodeEnvironment {

	constructor( config, context ) {
		super( config, context );
		this.testPath = context.testPath;
		this.docblockPragmas = context.docblockPragmas;
	}

	async setup() {
		await super.setup();
		if ( process.env.SPLITKEY ) {
			const rawData = fs.readFileSync( 'splitio.json' );
			this.global.SPLITS = JSON.parse( rawData );
		}
	}

}

module.exports = ReadSplits;
