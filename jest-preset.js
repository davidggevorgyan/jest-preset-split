module.exports = {
	globalSetup: 'jest-preset-split/src/get-splits.js',
	globalTeardown: 'jest-preset-split/src/teardown-splits.js',
	testEnvironment: 'jest-preset-split/src/read-splits.js',
};
