module.exports = {
	globalSetup: './src/get-splits',
	globalTeardown: './src/teardown-splits',
	testEnvironment: './src/read-splits',
};
