# jest-preset-split

[![npm](https://img.shields.io/npm/v/jest-preset-split)](https://www.npmjs.com/package/jest-preset-split)
[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/davidggevorgyan/jest-preset-split)](https://gitlab.com/davidggevorgyan/jest-preset-split/pipelines)

This jest-preset stores your toggles with treatments from split.io as a global variable `global.SPLITS` available in your tests.

## Installation

```sh-session
npm i jest-preset-split --save-dev
```

## Usage

To use this preset, add it to your jest config and update your test command to include `SPLITKEY` for the environment which information you want to get.

### Update Jest config

To make Jest use this preset, you need to update your Jest config and add `jest-preset-split` preset to it. For example, if your jest config is in the `package.json` file:

```json
{
    "name": "my-package",
    "version": "1.0.0",
    "dependencies": {
    },
    "jest": {
        "preset": "jest-preset-split"
    }
}
```

Or in the `jest.config.js` file:

```javascript
module.exports = {
    ...
    preset: "jest-preset-split"
};
```

### Getting splits and current treatments

Once you update your jest config, you can now get all current splits via `global.SPLITS` variable. Just specify a valid split environment SDK key like this:

```sh-session
// using npx:
SPLITKEY=<YOUR_SDK_KEY_HERE> npx jest

// or via npm:
SPLITKEY=<YOUR_SDK_KEY_HERE> npm test
```

Here is an example of `global.SPLITS` structure

```json
{
    'split-one': 'on',
    'split-two': 'off',
    'split-three': 'custom'
}
```

### Running tests based on splits

You can now filter tests by `describe` block:

```javascript
import Foo from '../src/Foo';
if(global.SPLITS['split-1'] === 'on') {
    describe( 'Foo class', () => {
        it( '...', () => {
            ...
        } );

        ...
    } );
}
```

Or you can have logic inside your test

```javascript
describe( 'Dashboard page', () => {
    it( '...', () => {
           if(global.SPLITS['split-1'] === 'on') {
                ...
           } else {
                ...
           }
        } );
} );
```

### Mocking splits in localhost mode

You can also use `global.SPLITS` to mock your environment splits in localhost mode

```javascript
var sdk = splitio({
  core: {
    authorizationKey: 'localhost'
  },
  features: global.SPLITS,
  scheduler: {
    offlineRefreshRate: 15 // 15 sec
  }
});
```

## Contribute

Want to help or have a suggestion? Open a [new ticket](https://gitlab.com/davidggevorgyan/jest-preset-split/-/issues/new) and we can discuss it or submit a pull request.

## License

MIT
