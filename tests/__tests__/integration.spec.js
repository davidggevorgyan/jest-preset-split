if ( global.SPLITS['feature-one'] === 'on' ) {
	test( 'should run the test if condition is true', () => {
		expect( 1 + 2 ).toBe( 3 );
	} );
}

if ( global.SPLITS['feature-one'] === 'off' ) {
	test( 'should skip the test if condition is false', () => {
		expect( 1 + 2 ).toBe( 100 );
	} );
}
