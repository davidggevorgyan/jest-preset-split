const getSplits = require( '../../src/get-splits' );


test( 'should resolve the promise when split API key in not provided', async () => {
	delete process.env.SPLITKEY;
	return expect( getSplits() ).resolves.toBe( 'No API Key found' );
} );
